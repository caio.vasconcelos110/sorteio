import java.util.Random;
import java.util.Scanner;

public class UsuarioMain {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Solicitar ao usuário que insira suas informações
        System.out.println("Por favor, insira suas informações:");
        System.out.print("ID: ");
        int id = scanner.nextInt();
        scanner.nextLine(); // Consumir a quebra de linha

        System.out.print("Consentimento (true/false): ");
        boolean consentimento = scanner.nextBoolean();
        scanner.nextLine(); // Consumir a quebra de linha

        System.out.print("Data de nascimento (yyyy-MM-dd): ");
        String dataNascimento = scanner.nextLine();

        System.out.print("Nome: ");
        String nome = scanner.nextLine();

        System.out.print("CPF: ");
        String cpf = scanner.nextLine();

        System.out.print("Email: ");
        String email = scanner.nextLine();

        System.out.print("Perfil: ");
        String perfil = scanner.nextLine();

        System.out.print("Role: ");
        String role = scanner.nextLine();

        System.out.print("Senha: ");
        String senha = scanner.nextLine();

        System.out.print("Telefone: ");
        String telefone = scanner.nextLine();

        System.out.print("Estado: ");
        String estado = scanner.nextLine();

        System.out.print("Município: ");
        String municipio = scanner.nextLine();

        System.out.print("CEP: ");
        String cep = scanner.nextLine();

        System.out.print("Bairro: ");
        String bairro = scanner.nextLine();

        System.out.print("Logradouro: ");
        String logradouro = scanner.nextLine();

        System.out.print("Número: ");
        String numero = scanner.nextLine();

        System.out.print("Complemento: ");
        String complemento = scanner.nextLine();

        // Solicitar ao usuário que insira o número do bilhete
        System.out.print("\nDigite o número do bilhete: ");
        int numeroBilhete = scanner.nextInt();

        // Emitir uma nota fiscal de 25 reais e obter o número da nota fiscal emitida
        int numeroNotaFiscal = emitirNotaFiscal(25.0);

        System.out.println("\nNota fiscal de R$" + 25.0 + " emitida com sucesso.");

        System.out.println("\nVocê emitiu o bilhete de número " + numeroBilhete + " de 1000.");

        // Realizar o sorteio e obter o número sorteado
        int numeroSorteado = realizarSorteio();

        // Exibir o número sorteado
        System.out.println("\nNúmero sorteado: " + numeroSorteado);

        // Verificar se o usuário ganhou ou não
        boolean ganhou = numeroSorteado == numeroBilhete;

        // Exibir o resultado do sorteio
        if (ganhou) {
            System.out.println("\nParabéns! Você ganhou o sorteio e recebeu o prêmio de R$25,00!");
        } else {
            System.out.println("\nQue pena! Você não ganhou o sorteio.");
        }

        scanner.close();
    }

    // Método para emitir uma nota fiscal com o valor especificado e retornar o número da nota fiscal emitida
    private static int emitirNotaFiscal(double valor) {
        Random random = new Random();
        int numeroNotaFiscal = random.nextInt(1000); // Número aleatório entre 0 e 999
        return numeroNotaFiscal;
    }

    // Método para realizar o sorteio e retornar o número sorteado
    private static int realizarSorteio() {
        Random random = new Random();
        return random.nextInt(1000) + 1; // Número aleatório entre 1 e 1000
    }
}
